package transaction.script.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.PostgresDBUtil;

public class AppConstants {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDBUtil.class);
	static Properties properties;
	static {
		properties = new Properties();
		try {
			InputStream inputStream = AppConstants.class.getClassLoader().getResourceAsStream("platform.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("<< Error loading properties file:");

		}
	}

	public static final String POSTGRES_IP = properties.getProperty("postgres_host");
	public static final String TRANSACTION = "TRANSACTION";
	//Dynamo Constants
	public static final String DYNAMO_ENVIRONMENT = properties.getProperty("dynamo_environment");
	public static final String DYNAMO_FROM = properties.getProperty("dynamo_from");
	public static final String DYNAMO_HOST = properties.getProperty("dynamo_host");
	public static final String AWS_ACCESSKEY = properties.getProperty("aws_accessKey");
	public static final String AWS_SECRETKEY = properties.getProperty("aws_secretKey");
	//Table Names for postgres
	public static final String TRADER_TABLE_NAME = "kal_traderapp_transaction";
	public static final String CA_TABLE_NAME = "kal_ca_transaction";
	public static final String FARMER_TABLE_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_TABLE_NAME = "kal_ai_transaction";
	public static final String SOCIETY_TABLE_NAME = "kal_cos_transaction";
	public static final String TRANSPORTER_TABLE_NAME = "kal_transporterapp_transaction";
	public static final String WAREHOUSER_TABLE_NAME = "kal_wh_transaction";
	//Collection Names for mongo
	public static final String TRADER_COLLECTION_NAME = "kal_traderapp_transaction";
	public static final String CA_COLLECTION_NAME = "kal_ca_transaction";
	public static final String FARMER_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_COLLECTION_NAME = "kal_fertilizerapp_transaction";
	public static final String SOCIETY_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String TRANSPORTER_COLLECTION_NAME = "kal_transporter_transaction";
	public static final String WAREHOUSER_COLLECTION_NAME = "kal_warehouseapp_transaction";
	//Biz Types
	public static final String TRADER = "traderapp";
	public static final String CA = "caapp";
	public static final String FARMER = "farmerapp";
	public static final String INPUTS = "aiapp";
	public static final String SOCIETY = "cosapp";
	public static final String TRANSPORTER = "transporterapp";
	public static final String WAREHOUSE = "warehouse";
	public static final String MONGO_HOST_ONE = properties.getProperty("mongo_host");
	public static final int MONGO_HOST_PORT = Integer.parseInt(properties.getProperty("mongo_port"));
	public static final String MONGO_HOST_TWO = properties.getProperty("mongo_host_two");
	public static final String MONGO_DATABASE = properties.getProperty("common_mongo_db_name");
	public static final String MONGO_USER = properties.getProperty("mongo_user");
	public static final String MONGO_PASSWORD = properties.getProperty("mongo_password");
	//Data Correction Constants
	public static final String DUPLICATE = "DUPLICATE";//Production status
	public static final String ACTIVE = "ACTIVE";//Production status
	public static final String DUPLICATE_ALL = "DUPLICATE_ALL";//Local mongo status
	public static final String DUPLICATE_DATA = "DUPLICATE_DATA";//Local mongo status
	public static final String DUPLICATE_MOBILE = "DUPLICATE_MOBILE";//Local mongo status
	public static final String IN_QUEUE = "IN_QUEUE";//Local mongo && production status
	public static final String FAIL_DUP_MOBILE = "FAIL_DUP_MOBILE";
	public static final String FAIL_EXCEPTION = "FAIL_EXCEPTION";
	public static final String DATA_SHEET = "DATA";
	//BusinessType Constants
	public static final String BUSINESS_TYPE_SHEETNAME_AGRITRADER = "agritrader";
	public static final String BUSINESS_TYPE_SHEETNAME_AGRIBROKER = "broker";
	public static final String BUSINESS_TYPE_SHEETNAME_FARMER = "farmer";
	public static final String BUSINESS_TYPE_SHEETNAME_TRANSPORTER = "transporter";
	public static final String BUSINESS_TYPE_SHEETNAME_AGRIINPUT = "agriinputs";
	public static final String BUSINESS_TYPE_SHEETNAME_AGRISOCIETY = "agricoop";
	public static final String BUSINESS_TYPE_SHEETNAME_WAREHOUSE = "warehouser";
	public static final String BUSINESS_TYPE_ID_AGRITRADER = "BT000000";
	public static final String BUSINESS_TYPE_ID_AGRIBROKER = "BT000001";
	public static final String BUSINESS_TYPE_ID_FARMER = "BT000002";
	public static final String BUSINESS_TYPE_ID_TRANSPORTER = "BT000003";
	public static final String BUSINESS_TYPE_ID_AGRIINPUT = "BT000008";
	public static final String BUSINESS_TYPE_ID_AGRISOCIETY = "BT000010";
	public static final String BUSINESS_TYPE_ID_WAREHOUSE = "BT000006";
	public static final String BUSINESS_TYPE_DBNAME_AGRITRADER = "Agri Trader";
	public static final String BUSINESS_TYPE_DBNAME_BROKER = "Broker (Intermediary)";
	public static final String BUSINESS_TYPE_DBNAME_FARMER = "Farmer";
	public static final String BUSINESS_TYPE_DBNAME_TRANSPORTER = "Transporter";
	public static final String BUSINESS_TYPE_DBNAME_WAREHOUSE = "Warehouser";
	public static final String BUSINESS_TYPE_DBNAME_INPUTS = "Agri Inputs";
	public static final String BUSINESS_TYPE_DBNAME_SOCIETY = "Agriculture Cooperative Society";
	//Post constants
	public static final String BUY_POST = "Buy";
	//Validation constants
	public static final String INVALID_NAME = "Name invalid-";
	public static final String INVALID_BIZ_NAME = "Biz name invalid-";
	public static final String INVALID_MOBILE = "Mobile invalid-";
	public static final String INVALID_BIZ_TYPE = "Biz type invalid-";
	public static final String INVALID_LOCATION = "Location invalid-";
	public static final String INVALID_PRODUCT = "Products invalid-";
	public static final String INVALID_DATA = "Data is duplicate.";
	public static final String INVALID_COUNTRY_NAME = "Country Name invalid-";
	public static final String INVALID_DESCRIPTION = "Description invalid-";
	public static final String INVALID_POST_TYPE = "Post type invalid-";
	public static final String INVALID_POST_PRODUCT = "Invalid post product-";
	@SuppressWarnings("serial")
	public static final Map<String, String> MOBILE_TELECOM_CODES_LENGTH_MAP = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put("+254", "9");
			put("+237", "9");
			put("+243", "9");
			put("+251", "9");
			put("+233", "9");
			put("+225", "9");
			put("+213", "9");
			put("+261", "9");
			put("+258", "9");
			put("+234", "10");
			put("+242", "9");
			put("+255", "9");
			put("+256", "9");
			put("+63", "10");
			put("+880", "10");
			put("+62", "9,10,11");
			put("+95", "8");
			put("+977", "10");
			put("+92", "10");
			put("+91", "10");
			put("+966", "9");
			put("+94", "7");
			put("+84", "10");
			put("+66", "9");
			put("+31", "9");
			put("+40", "9");
			put("+46", "9");
			put("+48", "9");
			put("+7", "10");
			put("+49", "11");
			put("+33", "9");
			put("+34", "9");
			put("+90", "11");
			put("+380", "9");
			put("+55", "10");
			put("+20", "10");
			put("+98", "10");
			put("+60", "9,10");
			put("+971", "10");
			put("+54", "10");
			put("+212", "9");
			put("+974", "8");
			put("+52", "10");
			put("+973", "8");
			put("+229", "8");
			put("+975", "8");
			put("+57", "10");
			put("+964", "10");
			put("+968", "8");
			put("+228", "8");
			put("+58", "10");
			put("+965", "8");

		}
	});

	@SuppressWarnings("serial")
	public static final Map<String, String> COUNTRY_CODES = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put("kenya", "+254");
			put("cameroon", "+237");
			put("democratic republic of the congo", "+243");
			put("ethiopia", "+251");
			put("ghana", "+233");
			put("ivory coast", "+225");
			put("algeria", "+213");
			put("madagascar", "+261");
			put("mozambique", "+258");
			put("nigeria", "+234");
			put("republic of the congo", "+242");
			put("tanzania", "+255");
			put("uganda", "+256");
			put("philippines", "+63");
			put("bangladesh", "+880");
			put("indonesia", "+62");
			put("myanmar", "+95");
			put("nepal", "+977");
			put("pakistan", "+92");
			put("india", "+91");
			put("saudi arabia", "+966");
			put("sri lanka", "+94");
			put("vietnam", "+84");
			put("thailand", "+66");
			put("netherlands", "+31");
			put("romania", "+40");
			put("sweden", "+46");
			put("poland", "+48");
			put("russian federation", "+7");
			put("germany", "+49");
			put("france", "+33");
			put("spain", "+34");
			put("turkey", "+90");
			put("ukraine", "+380");
			put("brazil", "+55");
			put("egypt", "+20");
			put("iran", "+98");
			put("malaysia", "+60");
			put("united arab emirates", "+971");
			put("argentina", "+54");
			put("morocco", "+212");
			put("qatar", "+974");
			put("mexico", "+52");
			put("bahrain", "+973");
			put("benin", "+229");
			put("bhutan", "+975");
			put("colombia", "+57");
			put("iraq", "+964");
			put("oman", "+968");
			put("togo", "+228");
			put("venezuela", "+58");
			put("kuwait", "+965");

		}
	});
	@SuppressWarnings("serial")
	public static final Map<String, String> COUNTRY_IDS = Collections.unmodifiableMap(new HashMap<String, String>() {
		{
			put("kenya", "101");
			put("cameroon", "107");
			put("democratic republic of the congo", "112");
			put("ethiopia", "117");
			put("ghana", "120");
			put("ivory coast", "123");
			put("algeria", "124");
			put("madagascar", "128");
			put("mozambique", "135");
			put("nigeria", "138");
			put("republic of the congo", "139");
			put("tanzania", "150");
			put("uganda", "153");
			put("philippines", "301");
			put("bangladesh", "303");
			put("indonesia", "314");
			put("myanmar", "329");
			put("nepal", "330");
			put("pakistan", "333");
			put("india", "335");
			put("saudi arabia", "336");
			put("sri lanka", "340");
			put("vietnam", "349");
			put("thailand", "377");
			put("netherlands", "436");
			put("romania", "440");
			put("sweden", "448");
			put("poland", "438");
			put("russian federation", "355");
			put("germany", "419");
			put("france", "417");
			put("spain", "446");
			put("turkey", "345");
			put("ukraine", "450");
			put("brazil", "603");
			put("egypt", "114");
			put("iran", "315");
			put("malaysia", "326");
			put("united arab emirates", "347");
			put("argentina", "601");
			put("morocco", "134");
			put("qatar", "313");
			put("mexico", "522");
			put("bahrain", "302");
			put("benin", "103");
			put("bhutan", "304");
			put("colombia", "605");
			put("iraq", "316");
			put("oman", "332");
			put("togo", "151");
			put("venezuela", "614");
			put("kuwait", "321");
		}
	});
}