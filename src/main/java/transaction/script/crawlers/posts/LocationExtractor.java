package transaction.script.crawlers.posts;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.platform.utils.HsqlDBUtil;

public class LocationExtractor {

	public static final Logger LOGGER = LoggerFactory.getLogger(ProductExtractor.class);
	public static final List<String> ignoredLettersAndWords = Collections.unmodifiableList(Arrays.asList(new String[] { ".", "@", "!", "#", "$", "%", "&", "~", "^", "*", "+", "-",
			"/", "\\", ":", ";", "'", "\"", "[", "]", "(", ")", "{", "}", "=", ",", "_", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "telephone", "post", "city",
			"industrial", "street", "port", "manor", "peoples", "international", "link", "floor", "road", "box", "park", "lane", "cross", "section", "house", "contact", "central",
			"plaza", "tower", "north", "south", "west", "east", "avenue", "highway", "lakeside", "quality", "middle", "triangle", "market", "centre", "college", "village",
			"centre", "center", "wall", "police", "cash", "beach", "king", "bridge", "island", "trees", "ferry", "commerce", "stadium", "mill", "flat" }));
	static HashMap<String, ArrayList<String>> locations = new HashMap<String, ArrayList<String>>();
	static ArrayList<String> countries = new ArrayList<String>();
	static Connection hsqlConnection = null;
	static LocationExtractor classObj = new LocationExtractor();
	static {
		hsqlConnection = HsqlDBUtil.dbConnection;
		classObj.getAllLocations();
		System.out.println("Initial data setup done!!");
	}

	public static void main(String[] args) {
		try {
			Workbook workbook = new XSSFWorkbook(new FileInputStream("/home/abhi/Desktop/postsbuy-agrixchange.xlsx"));
			Sheet sheet = workbook.getSheet(AppConstants.DATA_SHEET);
			System.out.println("Sheet loaded!!");
			int i = 0;
			for (Row row : sheet) {
				System.out.println(row.getRowNum());
				String data = row.getCell(4).getStringCellValue();
				for (String letter : ignoredLettersAndWords) {
					data = data.toLowerCase().replace(letter, " ");
				}
				ArrayList<String> dataWithCountryDetails = classObj.extractAndRemoveAllCountryNames(data);
				ArrayList<String> locationDetails = null;
				if (dataWithCountryDetails.size() > 1)
					locationDetails = classObj.extractLocationFromData(dataWithCountryDetails.get(0), dataWithCountryDetails.get(1));
				else
					locationDetails = classObj.extractLocationFromData(dataWithCountryDetails.get(0), null);
				if (locationDetails.size() > 0) {
					row.createCell(12).setCellValue(locationDetails.get(0));
					if (locationDetails.size() > 1) {
						row.createCell(13).setCellValue(locationDetails.get(1));
					} else if (dataWithCountryDetails.size() > 1 && dataWithCountryDetails.get(1) != null && !dataWithCountryDetails.get(1).isEmpty()) {
						row.createCell(13).setCellValue(dataWithCountryDetails.get(1));
					}
				} else {
					if (dataWithCountryDetails.size() > 1 && dataWithCountryDetails.get(1) != null && !dataWithCountryDetails.get(1).isEmpty()) {
						row.createCell(13).setCellValue(dataWithCountryDetails.get(1));
					}
					i++;
					row.createCell(14).setCellValue(dataWithCountryDetails.get(0).trim());
				}
			}
			System.out.println("hhhhhhh" + i);
			workbook.write(new FileOutputStream("/home/abhi/Desktop/postsbuy-agrixchange.xlsx"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private ArrayList<String> extractLocationFromData(String data, String extractedCountryName) {
		ArrayList<String> locationDetails = new ArrayList<String>();
		for (String word : data.split(" ")) {
			ArrayList<String> countryIds = locations.get(word.toLowerCase().trim());
			if (countryIds != null) {
				locationDetails.add(word.toLowerCase().trim());
				if (extractedCountryName != null)
					locationDetails.add(extractedCountryName);
				else {
					if (countryIds.size() == 1) {
						for (String countryName : AppConstants.COUNTRY_IDS.keySet()) {
							if (AppConstants.COUNTRY_IDS.get(countryName).equals(countryIds.get(0)))
								locationDetails.add(countryName);
						}
					}
				}
			}
		}
		/*if (locationDetails.isEmpty()) {
			for (String area : locations.keySet()) {
				if (data.toLowerCase().contains(area)) {
					for (String countryName : AppConstants.COUNTRY_IDS.keySet()) {
						if (area.equals(countryName.toLowerCase())) {
							locationDetails.add(area);
							locationDetails.add(countryName);
						}
					}
					if (!locationDetails.isEmpty())
						break;
				}
			}
		}*/
		return locationDetails;
	}

	public void getAllLocations() {
		try {
			Statement statement = hsqlConnection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT LOCATION_NAME,ALSO_KNOWN_AS,LOCATION_ID FROM LOCATIONS");
			while (resultSet.next()) {
				String locationName = resultSet.getString("LOCATION_NAME").toLowerCase();
				String countryId = resultSet.getString("LOCATION_ID").trim().substring(0, 3);
				if (locationName.trim().length() < 4)
					continue;
				ArrayList<String> countryIds = new ArrayList<String>();
				locationName = locationName.toLowerCase().trim();
				if (!locations.containsKey(locationName)) {
					countryIds.add(countryId);
					locations.put(locationName, countryIds);
				} else {
					countryIds = locations.get(locationName);
					countryIds.add(countryId);
					locations.put(locationName, countryIds);
				}
				String locationAlsoKnownAs = resultSet.getString("ALSO_KNOWN_AS");
				if (locationAlsoKnownAs != null && !locationAlsoKnownAs.isEmpty()) {
					for (String otherName : locationAlsoKnownAs.split(",")) {
						if (otherName != null && !otherName.isEmpty()) {
							otherName = otherName.toLowerCase().trim();
							if (!locations.containsKey(otherName)) {
								countryIds.add(countryId);
								locations.put(otherName, countryIds);
							} else {
								countryIds = locations.get(otherName);
								countryIds.add(countryId);
								locations.put(otherName, countryIds);
							}
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	private ArrayList<String> extractAndRemoveAllCountryNames(String data) {
		ArrayList<String> countryDetailsWithData = new ArrayList<String>();
		countryDetailsWithData.add(data);
		try {
			if (countries.isEmpty()) {
				Statement statement = hsqlConnection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM COUNTRIES");
				while (resultSet.next()) {
					countries.add(resultSet.getString("COUNTRY_NAME").toLowerCase().trim());
				}
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
			}
			for (String country : countries) {
				if (data.toLowerCase().contains(country)) {
					data = data.toLowerCase().replace(country, " ");
					countryDetailsWithData.add(0, data);
					countryDetailsWithData.add(1, country);
					break;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return countryDetailsWithData;
	}
}