/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;

public class DynamoDBApiImpl implements DyanmoDBAPI {
	static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(DynamoDBApiImpl.class);

	public String getValue(String moduleId, String key) throws Exception {
		moduleId = DynamoConstants.DYNAMO;
		DynamoDB dynamoDB = null;
		try {
			dynamoDB = new DynamoDButil().getConnection();
			Table table = dynamoDB.getTable(moduleId);
			Item item = table.getItem("Id", key);
			if (item != null) {
				LOGGER.info("<< Object returned successfully from dynamoDB");
				return (String) item.get("Data");
			} else {
				return null;
			}
		} catch (Exception e) {
			LOGGER.error("<< Exception occured with dynamoDB");
			throw new Exception(e.getMessage());
		} finally {
			LOGGER.info("<< Connection closed successfully with dynamoDB");
			dynamoDB.shutdown();
		}
	}

	public int createOrReplaceValue(String moduleId, String operationName, String data, String key) throws Exception {
		moduleId = DynamoConstants.DYNAMO;
		operationName = DynamoConstants.DYNAMO;
		DynamoDB dynamoDB = null;
		try {
			dynamoDB = new DynamoDButil().getConnection();
			Table table = dynamoDB.getTable(moduleId);
			Item item = new Item();
			item.withPrimaryKey("Id", key);
			item.withString("KeyPatternId", key);
			item.withString("Operation", operationName);
			item.withString("Data", data);
			table.putItem(item);
			LOGGER.info("<< Value with the associted key is updated or created in dynamoDB");
			return 1;
		} catch (Exception e) {
			LOGGER.error("<< Module Id:{} and data:{}", moduleId, data);
			LOGGER.error("<< Data is: {}", data);
			LOGGER.error("<< Exception occured with dynamoDB");
			throw new Exception();
		} finally {
			LOGGER.info("<< Connection closed successfully with dynamoDB");
			dynamoDB.shutdown();
		}
	}
}