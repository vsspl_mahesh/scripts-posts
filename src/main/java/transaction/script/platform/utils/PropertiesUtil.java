/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertiesUtil {
	private Pattern pattern = Pattern.compile("\\{.*?\\}");
	private Properties properties;

	public PropertiesUtil() {
		properties = new Properties();
	}

	public void load(InputStream inputStream) throws IOException {
		properties.load(inputStream);
	}

	public String getProperty(String key) {
		if (key == null) {
			return null;
		}
		String value = properties.getProperty(key);
		if (value == null) {
			return "";
		}
		String result = new String(value);
		Matcher matcher = pattern.matcher(value);
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();
			result = result.replace(value.substring(start, end), getProperty(value.substring(start + 1, end - 1)));
		}
		return result.trim();
	}
}