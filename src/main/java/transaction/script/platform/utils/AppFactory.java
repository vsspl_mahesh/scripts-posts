/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

public class AppFactory {

	private static DyanmoDBAPI dynamoApi = new DynamoDBApiImpl();

	public static DyanmoDBAPI getDynamoApi() {
		return dynamoApi;
	}
}